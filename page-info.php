<?php
/*
    Template Name: Info
*/
get_header(); ?>

<main class="about-content">
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

    <article>
    <?php if ( have_rows( 'content_blocks' ) ): ?>
        <?php while ( have_rows( 'content_blocks' ) ) : the_row(); ?>
            <?php if ( get_row_layout() == 'lead_text' ) : ?>

                <div class="container info-block">
                    <p class="lead-text"><?php the_sub_field( 'text' ); ?></p>
                </div>

            <?php elseif ( get_row_layout() == 'full_width_image' ) : ?>

                <div class="full-width-container info-block">
                    <?php $image = get_sub_field( 'image' ); ?>
                    <?php if ( $image ) { ?>
                        <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                    <?php } ?>
                </div>

            <?php elseif ( get_row_layout() == 'text_+_sticky_image' ) : ?>

                <div class="container info-block">
                    <div class="row section-1">
                        <div class="column-text">
                            <?php the_sub_field( 'content' ); ?>
                        </div>
                        <div class="column-image">
                            <?php $sticky_image = get_sub_field( 'sticky_image' ); ?>
                            <?php if ( $sticky_image ) { ?>
                                <?php echo wp_get_attachment_image( $sticky_image, 'full' ); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php elseif ( get_row_layout() == 'sticky_image_+_text' ) : ?>

                <div class="container info-block">
                    <div class="row section-2">
                        <div class="column-image">
                            <?php $sticky_image = get_sub_field( 'sticky_image' ); ?>
                            <?php if ( $sticky_image ) { ?>
                                <?php echo wp_get_attachment_image( $sticky_image, 'full' ); ?>
                            <?php } ?>
                        </div>
                        <div class="column-text">
                            <?php the_sub_field( 'content' ); ?>
                        </div>
                    </div>
                </div>

            <?php endif; ?>
        <?php endwhile; ?>
    <?php else: ?>
        <?php // no layouts found ?>
    <?php endif; ?>
    </article>
</main>

<?php get_footer(); ?>
