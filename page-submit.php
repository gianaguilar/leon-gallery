<?php
/*
    Template Name: Submit Artwork
*/
get_header(); ?>

<main>
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

    <section class="section submit-artwork">
        <div class="container">
            <header>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <?php the_content (); ?>
                <?php endwhile; ?>
                <!-- post navigation -->
                <?php else: ?>
                <!-- no posts found -->
                <?php endif; ?>
            </header>
            <div>
                <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 2, 'title' => false, 'description' => false ) ); ?>
            </div>
        </div>
    </section>
</main>

<footer class="site-footer">
    <div class="site-info">
        <div class="info-box">
            <?php the_field( 'info_box_3', 'option' ); ?>

            <a href="<?php echo esc_url( home_url( '/info' ) ); ?>" class="btn">Learn More</a>
        </div>
        <div class="info-box">
            <?php the_field( 'info_box_1', 'option' ); ?>
        </div>
    </div>

    <div class="colophon">
        <div>
            <div class="copyright">&copy; <?php echo date('Y') ?> León Gallery</div>
            <div>Designed with &hearts; by vgrafiks</div>
        </div>
    </div>
</footer>


<?php wp_footer(); ?>

</body>
</html>
