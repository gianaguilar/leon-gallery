<?php get_header(); ?>

<main>
    <div>
        <?php get_template_part('template-parts/components/top', 'bar' ); ?>
        <?php get_template_part('template-parts/components/auction', 'hero'); ?>
    </div>

    <div class="wrapper">
        <div id="target" class="fs-wrapper">
            <?php get_template_part('template-parts/components/fs', 'bar'); ?>
        </div>

        <section class="section previous-auctions">
            <header class="search-header">
                <h4 class="search-title"></h4>
                <div class="search-results"></div>
            </header>

            <div class="index-container">
                    <div class="box-index">
                    <?php
                        $term_list = wp_get_post_terms($post->ID, 'auction-catalog', array("fields" => "all"));
                        foreach($term_list as $term_single);

                        // Query Arguments
                        $args = array(
                            'post_type' => array('lots'),
                            'posts_per_page' => 40,
                            'order' => 'ASC',
                            'orderby' => 'date',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'auction-catalog',
                                    'field' => 'slug',
                                    'terms' => $term_single->slug,
                                ),
                            ),
                        );

                        // The Query
                        $query_lots = new WP_Query( $args );

                        // The Loop
                        if ( $query_lots->have_posts() ) {
                            while ( $query_lots->have_posts() ) {
                                $query_lots->the_post(); ?>
                                    <div class="box box-quarters">
                                    <a class="box-link" href="<?php the_permalink(); ?>">
                                        <div class="box-img">
                                            <?php if ( has_post_thumbnail() ) { ?>
                                                <?php the_post_thumbnail('box-thumb-hard'); ?>
                                            <?php } else { ?>

                                                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/1x1.svg';?>" alt="No Image Found">

                                            <?php } ?>
                                        </div>


                                        <div class="box-info">
                                            <div class="box-lot-num">Lot #<?php the_field( 'lot_number' ); ?></div>

                                            <h3 class="box-title lot-title">
                                                <?php the_field( 'artist' ); ?>
                                            </h3>

                                            <div class="lot-artwork-title"><?php the_field('artwork_title'); ?></div>
                                            <div class="box-asterisk"></div>

                                            <div class="box-starting-bid">
                                                <h4>Starting Bid</h4>
                                                <div class="sb-price">
                                                <?php
                                                        $str = get_field('starting_bid');

                                                        if (preg_match('#[0-9]#',$str)){
                                                            echo 'Php ' . number_format((get_field('starting_bid')), 0, '.', ',');
                                                        } else {
                                                            echo get_field('starting_bid');
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    </div>
                            <?php }
                        } else {
                            // no posts found
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();
                    ?>
                    </div>
            </div>
        </section>
    </div>
</main>

<?php get_footer(); ?>