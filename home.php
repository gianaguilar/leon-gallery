<?php
/*
    Template Name: Home
*/
get_header(); ?>

<main>
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

    <?php get_template_part('template-parts/components/auction', 'flexslider') ?>

    <section class="section previous-auctions">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title">Previous Auctions</h2>
            </header><!-- /header -->

            <div class="box-container carousel">
                <?php
                    // Query Arguments
                    $args = array(
                        'post_type' => array('auction'),
                        'posts_per_page' => 3,
                        'order' => 'DESC',
                    );

                    // The Query
                    $query_auctions = new WP_Query( $args );

                    // The Loop
                    if ( $query_auctions->have_posts() ) {
                        while ( $query_auctions->have_posts() ) {
                            $query_auctions->the_post(); ?>
                                <div class="box box-thirds">
                                    <a class="box-img" href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>

                                            <?php the_post_thumbnail( 'box-thumb-hard' ); ?>

                                        <?php } else { ?>

                                            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/1x1.svg';?>" alt="No Image Found">

                                        <?php } ?>
                                    </a>
                                    <h3 class="box-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <div class="box-date"><?php the_field('date'); ?></div>
                                    <div class="box-time"><?php the_field('time'); ?></div>
                                    <div class="box-location"><?php the_field('address'); ?></div>
                                </div>
                        <?php }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                ?>

                <div class="box box-thirds no-height"></div>
                <div class="box box-thirds no-height"></div>
                <div class="box box-thirds no-height"></div>
            </div>

            <a href="<?php echo esc_url( home_url( '/auctions' ) ); ?>" class="btn">View All</a>
        </div>
    </section>

    <section class="section what-we-do">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title">What We Do</h2>
            </header><!-- /header -->

            <div class="box-container info-section">
                <div class="box box-halves">
                    <?php if ( have_rows( 'how_to_buy', 'option' ) ) : ?>
                        <?php while ( have_rows( 'how_to_buy', 'option' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'image' ) ) { ?>
                                <a class="box-img" href="<?php echo esc_url( home_url( '/info/#how-to-buy' ) ); ?>">
                                    <img src="<?php the_sub_field( 'image' ); ?>" />
                                </a>
                            <?php } ?>
                            <h3 class="box-title-lg"><a href="<?php echo esc_url( home_url( '/info' ) ); ?>"><?php the_sub_field( 'title' ); ?></a></h3>
                            <p class="box-text"><?php the_sub_field( 'text' ); ?></p>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>

                <div class="box box-halves">
                    <?php if ( have_rows( 'how_to_sell', 'option' ) ) : ?>
                        <?php while ( have_rows( 'how_to_sell', 'option' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'image' ) ) { ?>
                                <a class="box-img" href="<?php echo esc_url( home_url( '/info/#how-to-sell' ) ); ?>">
                                    <img src="<?php the_sub_field( 'image' ); ?>" alt="How to Sell"/>
                                </a>
                            <?php } ?>
                            <h3 class="box-title-lg"><a href="<?php echo esc_url( home_url( '/info' ) ); ?>"><?php the_sub_field( 'title' ); ?></a></h3>
                            <p class="box-text"><?php the_sub_field( 'text' ); ?></p>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <a href="<?php echo esc_url( home_url( '/info/#what-we-do' ) ); ?>" class="btn">Learn More</a>
        </div>
    </section><!-- /.what-we-do -->

    <section class="section journal">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title">Journal</h2>
            </header><!-- /header -->

            <div class="box-container">
                <?php
                    // Query Arguments
                    $args = array(
                        'posts_per_page' => 3,
                        'order' => 'DESC',
                    );

                    // The Query
                    $query_journal = new WP_Query( $args );

                    // The Loop
                    if ( $query_journal->have_posts() ) {
                        while ( $query_journal->have_posts() ) {
                            $query_journal->the_post(); ?>
                                <div class="box box-thirds">
                                    <a class="box-img" href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>

                                            <?php the_post_thumbnail('box-thumb-hard'); ?>

                                        <?php } else { ?>

                                            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/1x1.svg';?>" alt="No Image Found">

                                        <?php } ?>
                                    </a>
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="box-category"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></div>
                                        <h3 class="box-title"><?php the_title(); ?></h3>
                                        <div class="box-date-journal"><?php the_time('F j, Y');?></div>
                                        <?php if( get_field('author') ): ?>
                                            <div class="box-author">By <?php the_field( 'author' ); ?></div>
                                        <?php endif; ?>
                                        <?php if( get_field('excerpt_content') ): ?>
                                        <div class="box-excerpt">
                                            <p><?php the_field( 'excerpt_content' ); ?></p>
                                        </div>
                                        <?php endif; ?>
                                    </a>
                                </div>
                        <?php }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                ?>
            </div>

            <a href="<?php echo esc_url( home_url( '/journal' ) ); ?>" class="btn">View All Posts</a>
        </div>
    </section><!-- /.journal -->
</main>

<?php get_footer(); ?>
