<?php
get_header(); ?>

<main class="journal-entry">
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <section class="section">
        <div class="container">
            <header>
                <p><?php $category = get_the_category(); echo $category[0]->cat_name; ?></p>
                <h2 class="entry-title"><?php the_title(); ?></h2>
                <p class="entry-meta">
                    <?php if( get_field('author') ): ?>
                        Posted by <span><?php the_field( 'author' ); ?></span>
                    <?php endif; ?>
                    <br><?php the_time('F j, Y');?></p>
            </header>
        </div>
    </section>


    <section>
        <?php if ( have_rows( 'content_blocks' ) ): ?>
            <?php while ( have_rows( 'content_blocks' ) ) : the_row(); ?>
                <?php if ( get_row_layout() == 'full_width_image_block' ) : ?>
                    <?php if ( get_sub_field( 'full_width_image' ) ) { ?>
                        <div class="full-width-container block">
                            <img src="<?php the_sub_field( 'full_width_image' ); ?>" />
                        </div>
                    <?php } ?>
                <?php elseif ( get_row_layout() == 'content_block' ) : ?>
                    <div class="container content-container block">
                        <?php the_sub_field( 'content' ); ?>
                    </div>
                <?php elseif ( get_row_layout() == 'blockquote' ) : ?>
                    <div class="blockquote-container block">
                        <blockquote><?php the_sub_field( 'blockquote' ); ?></blockquote>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php else: ?>
            <?php // no layouts found ?>
        <?php endif; ?>
    </section>
    <?php endwhile; ?>

    <nav class="post-nav container">
        <div>
            <a class="btn" href="<?php echo esc_url( home_url( '/journal' ) ); ?>">Back to Index</a>
        </div>
    </nav>


    <?php else: ?>
    <!-- no posts found -->
    <?php endif; ?>
</main>

<?php get_footer(); ?>
