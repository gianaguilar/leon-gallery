<?php
/*
    Template Name: Auctions
*/
get_header(); ?>


<main>
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

<?php get_template_part('template-parts/components/auction', 'flexslider') ?>

    <section class="section previous-auctions">
        <div class="index-container">
            <header>
                <h2 class="section-title">Previous Auctions</h2>
            </header><!-- /header -->

            <div class="box-container carousel">
                <?php echo do_shortcode( '[ajax_load_more container_type="div" css_classes="auction-listing" preloaded="true" preloaded_amount="9" seo="true" repeater="template_1" post_type="auction" posts_per_page="9" pause="true" scroll="false" button_label="Load More" button_loading_label="Loading..."]' ); ?>

                <div class="box box-thirds no-height"></div>
                <div class="box box-thirds no-height"></div>
                <div class="box box-thirds no-height"></div>
            </div>
        </div>
    </section>
</main>


<?php get_footer(); ?>