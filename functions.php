<?php
/**
 * León Gallery functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Leon_Gallery
 */

if ( ! function_exists( 'leon_gallery_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function leon_gallery_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on León Gallery, use a find and replace
		 * to change 'leon-gallery' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'leon-gallery', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
        // add_image_size( ‘name-of-size’, width, height, crop mode );
        add_image_size( 'box-thumb-hard', 350, 350, true ); // Hard Crop Mode
        add_image_size( 'box-thumb-soft', 350, 350 ); // Soft Crop Mode
        // add_image_size( 'homepage-thumb', 220, 180 ); // Soft Crop Mode
        // add_image_size( 'singlepost-thumb', 590, 9999 ); // Unlimited Height Mode

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'leon-gallery' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'leon_gallery_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'leon_gallery_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function leon_gallery_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'leon_gallery_content_width', 640 );
}
add_action( 'after_setup_theme', 'leon_gallery_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function leon_gallery_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'leon-gallery' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'leon-gallery' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'leon_gallery_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function leon_gallery_scripts() {
	wp_enqueue_style( 'leon-gallery-style', get_stylesheet_uri() );
    wp_enqueue_style( 'leon-custom', get_template_directory_uri() . '/styles/css/app.css' );

    wp_enqueue_script('jquery');
	// wp_enqueue_script( 'leon-gallery-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	// wp_enqueue_script( 'leon-gallery-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script( 'leon-flexslider', get_template_directory_uri() . '/js/flexslider.js', array(), '2017', true );
    wp_enqueue_script( 'leon-headroom', get_template_directory_uri() . '/js/headroom.js', array(), '2017', true );
    wp_enqueue_script( 'leon-headroom-jquery', get_template_directory_uri() . '/js/jQuery.headroom.js', array(), '2017', true );
    wp_enqueue_script( 'leon-magnific', get_template_directory_uri() . '/js/magnific.js', array(), '2017', true );
    wp_enqueue_script( 'leon-selectric', get_template_directory_uri() . '/js/selectric.js', array(), '2017', true );
    wp_enqueue_script( 'leon-app', get_template_directory_uri() . '/js/min/app-min.js', array(), '2017', true );

    if(is_singular( 'auction' ) || is_singular('lots')) {
        wp_enqueue_script( 'leon-sticky-filter', get_template_directory_uri() . '/js/sticky-filter.js', array(), '2017', true );
    }

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'leon_gallery_scripts' );


/* Custom template tags for this theme. */
// require get_template_directory() . '/inc/template-tags.php';

/* Functions which enhance the theme by hooking into WordPress. */
require get_template_directory() . '/inc/template-functions.php';

/* Customizer additions. */
require get_template_directory() . '/inc/customizer.php';

/* Load Jetpack compatibility file. */
// if ( defined( 'JETPACK__VERSION' ) ) { require get_template_directory() . '/inc/jetpack.php'; }


// ACF Options Page
if ( function_exists('acf_add_options_page')) {
    acf_add_options_page();
    acf_add_options_sub_page('Global Options');
    // acf_add_options_sub_page('New Sub Page');
}