<?php
/*
    Template Name: Home
*/
get_header(); ?>

<main>
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

    <div class="container text-center">
    <h1>404</h1>
    <p>Page not found</p>
    <a class="btn" href="<?php echo esc_url( home_url( '/' ) ); ?>">Back to Home</a>
    </div>
</main>

<hr>

<?php get_footer(); ?>
