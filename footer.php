
<footer class="site-footer">
    <div class="site-info">
        <div class="info-box">
            <?php the_field( 'info_box_1', 'option' ); ?>
        </div>
        <div class="info-box">
            <?php the_field( 'info_box_2', 'option' ); ?>

            <a href="<?php echo esc_url( home_url( '/request-for-estimate' ) ); ?>" class="btn">Request Form</a>
        </div>
    </div>

    <div class="colophon">
        <div>
            <div class="copyright">&copy; <?php echo date('Y') ?> León Gallery</div>
            <div>Designed with &hearts; by vgrafiks</div>
        </div>
    </div>
</footer>
<div class="fs-bg"></div>


<?php wp_footer(); ?>
</body>
</html>
