<?php if(has_post_thumbnail()) { ?>

    <div class="hero-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
        <?php $lot_images_images = get_field( 'lot_images' ); ?>
        <?php if ( $lot_images_images ) :  ?>

            <a href="#magnific-gallery" class="gallery-link" >
                <?php get_template_part('img/icon', 'search' ); ?> View Gallery
            </a>
            <div id="magnific-gallery" class="hidden">
                <?php foreach ( $lot_images_images as $lot_images_image ): ?>
                    <a href="<?php echo $lot_images_image['url']; ?>"></a>
                <?php endforeach; ?>
            </div>

        <?php endif; ?>
    </div>

<?php } else { ?>

    <div class="hero-image" style="background-image: url('<?php echo esc_url( get_template_directory_uri()) . '/img/1x1.svg';?>')">
    </div>

<?php }; ?>