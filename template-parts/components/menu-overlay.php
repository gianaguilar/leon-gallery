<div id="menu-overlay">
    <div class="overlay-inner">
        <nav class="main-menu">
            <ul>
                <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                <li><a href="<?php echo esc_url( home_url( '/auctions' ) ); ?>">Auctions</a></li>
                <li><a href="<?php echo esc_url( home_url( '/info' ) ); ?>">Info</a></li>
                <li><a href="<?php echo esc_url( home_url( '/journal' ) ); ?>">Journal</a></li>
                <li><a href="<?php echo esc_url( home_url( '/get-in-touch' ) ); ?>">Get in Touch</a></li>
                <li><a href="<?php echo esc_url( home_url( '/request-for-estimate' ) ); ?>">Request for Estimate</a></li>
            </ul>
        </nav>

        <div class="contact-info">
            <ul>
                <li>(+632) 856 278</li>
                <li><a href="mailto:info@leon-gallery.com">info@leon-gallery.com</a></li>
            </ul>
        </div>
    </div>
</div>