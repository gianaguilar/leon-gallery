<div id="top-bar" class="top-bar">
    <div class="top-bar-logo">
        <h1 class="site-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">León Gallery</a></h1>
    </div>

    <?php if( get_field('ticker_text') ): ?>
        <div class="top-bar-marquee">
            <div class="marquee">
              <div>
                <span><?php the_field( 'ticker_text' ); ?></span>
                <span><?php the_field( 'ticker_text' ); ?></span>
              </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="top-bar-menu">
        <button class="btn-menu" href="#menu-overlay">
            <span class="hamburger"><span></span></span>
        </button>
    </div>

    <?php if ( is_singular('auction') || is_singular('lots') ) { ?>
        <div class="mobile-triggers">
            <button class="fs-trigger"></button>
        </div>
    <?php } ?>
</div>
