<div class="hero-image">
    <?php
    $images = get_field('lot_images');
    $size = 'full'; // (thumbnail, medium, large, full or custom size)

    if( $images ): ?>
    <div class="flexslider lot-slider">
        <ul class="slides popup-gallery">
            <?php foreach( $images as $image ): ?>
                <li class="lot-image">
                    <a href="<?php echo $image['url']; ?>" class="gallery-link">
                        <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
</div>