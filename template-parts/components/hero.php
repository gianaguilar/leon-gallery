<!-- Hero Type 1 | Full Slider -->
<?php if ( have_rows( 'hero_type_1' ) ) : ?>
    <?php while ( have_rows( 'hero_type_1' ) ) : the_row(); ?>
        <?php $image = get_sub_field( 'image' ); ?>
        <?php if ( $image ) { ?>
            <?php echo wp_get_attachment_image( $image, 'full' ); ?>
        <?php } ?>
        <?php the_sub_field( 'code' ); ?>
        <?php the_sub_field( 'date' ); ?>
        <?php the_sub_field( 'time' ); ?>
        <?php the_sub_field( 'address' ); ?>
    <?php endwhile; ?>
<?php endif; ?>



<!-- Hero Type 2 | Auction hero | Image on the left and Info on the right -->
<?php if ( have_rows( 'hero_type_2' ) ) : ?>
    <?php while ( have_rows( 'hero_type_2' ) ) : the_row(); ?>

        <section class="hero">
            <div class="hero-image">
                <?php $image = get_sub_field( 'image' ); ?>
                <?php if ( $image ) { ?>
                    <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                <?php } ?>
            </div>
            <div class="hero-auction-info">
                <div>
                    <?php if( get_sub_field('code') ): ?>
                        <div class="hero-code"><?php the_sub_field( 'code' ); ?></div>
                    <?php endif; ?>

                    <h2 class="hero-auction-title"><?php the_title(); ?></h2>

                    <div class="auction-details">
                        <?php if( get_sub_field('date') ): ?>
                            <div class="hero-date"><?php the_sub_field( 'date' ); ?></div>
                        <?php endif; ?>

                        <?php if( get_sub_field('time') ): ?>
                            <div class="hero-time"><?php the_sub_field( 'time' ); ?></div>
                        <?php endif; ?>

                        <?php if( get_sub_field('address') ): ?>
                            <div class="hero-location"><?php the_sub_field( 'address' ); ?></div>
                        <?php endif; ?>
                    </div>

                    <ul class="auction-nav">
                        <li>
                            <a href="#target" class="scrollTarget hero-auction-browse-lots btn">Browse Lots</a>
                        </li>

                        <?php if ( get_sub_field( 'downloadable_pdf' ) ) { ?>
                        <li>
                            <a href="<?php the_sub_field( 'downloadable_pdf' ); ?>" class="hero-auction-view-catalogue btn" target="_blank" download>Download PDF</a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </section><!-- /.hero -->
    <?php endwhile; ?>
<?php endif; ?>


<!-- Hero Type 3 | Lot hero | Image slider on the left and info on the right -->
<?php if ( have_rows( 'hero_type_3' ) ) : ?>
    <?php while ( have_rows( 'hero_type_3' ) ) : the_row(); ?>
        <section class="hero-auction-slider">
            <div class="hero-image">
                <?php

                $lot_images_images = get_sub_field( 'lot_images' );

                if ( $lot_images_images ) :  ?>
                    <div id="slider" class="flexslider">
                        <ul class="slides">
                            <?php foreach ( $lot_images_images as $lot_images_image ): ?>
                                <li class="slide-item">
                                    <a href="<?php echo $lot_images_image['url']; ?>">
                                        <img class="lot-image" src="<?php echo $lot_images_image['sizes']['thumbnail']; ?>" alt="<?php echo $lot_images_image['alt']; ?>" />
                                    </a>
                                    <!-- <p><?php // echo $lot_images_image['caption']; ?></p> -->
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>

            <div class="hero-info">
                <div>
                    <?php if( get_sub_field('lot_number') ): ?>
                        <div class="lot-info lot-code">Lot #<?php the_sub_field( 'lot_#' ); ?></div>
                    <?php endif; ?>

                    <?php if( get_sub_field('artist') ): ?>
                        <h2 class="lot-info lot-artist"><?php the_sub_field( 'artist' ); ?></h2>
                    <?php endif; ?>

                    <div class="lot-info lot-title"><?php the_sub_field( 'artwork_title' ); ?></div>

                    <?php if( get_sub_field('artwork_specs') ): ?>
                        <div class="lot-info hero-specs">
                            <?php the_sub_field( 'artwork_specs' ); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_sub_field('starting_bid') ): ?>
                        <div class="lot-info hero-starting-bid">Starting Bid : Php <?php the_sub_field( 'starting_bid' ); ?></div>
                    <?php endif; ?>


                    <?php if( get_sub_field('authenticity') || get_sub_field('provenance') || get_sub_field('exhibited') || get_sub_field('literature') ): ?>

                        <div class="lot-info hero-lot-details">
                            <?php if( get_sub_field('authenticity') ): ?>
                                <div class="lot-authenticity">
                                    <strong>Authenticity</strong>
                                    <?php the_sub_field( 'authenticity' ); ?>
                                </div>
                            <?php endif; ?>

                            <?php if( get_sub_field('provenance') ): ?>
                                <div class="lot-provenance">
                                    <strong>Provenance</strong>
                                    <?php the_sub_field( 'provenance' ); ?>
                                </div>
                            <?php endif; ?>

                            <?php if( get_sub_field('exhibited') ): ?>
                                <div class="lot-exhibited">
                                    <strong>Exhibited</strong>
                                    <?php the_sub_field( 'exhibited' ); ?>
                                </div>
                            <?php endif; ?>

                            <?php if( get_sub_field('literature') ): ?>
                                <div class="lot-literature">
                                    <strong>Literature</strong>
                                    <?php the_sub_field( 'literature' ); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_sub_field('writeup') ): ?>
                        <div class="lot-info hero-writeup">
                            <?php the_sub_field( 'writeup' ); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php endif; ?>


<!-- Hero Type 4 | Page Banner -->
<?php if ( have_rows( 'hero_type_4' ) ) : ?>
    <?php while ( have_rows( 'hero_type_4' ) ) : the_row(); ?>
        <?php $image = get_sub_field( 'image' ); ?>
        <?php if ( $image ) { ?>
            <?php echo wp_get_attachment_image( $image, 'full' ); ?>
        <?php } ?>
    <?php endwhile; ?>
<?php endif; ?>