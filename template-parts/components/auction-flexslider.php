<section class="hero">
    <?php
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'auction' ),
            'order'                  => 'DESC',
            'orderby'                => 'date',
            'posts_per_page'         => '1',
        );

        // The Query
        $query_auctions = new WP_Query( $args );

        // The Loop
        if ( $query_auctions->have_posts() ) {
            while ( $query_auctions->have_posts() ) {
                $query_auctions->the_post(); ?>

                        <?php if ( has_post_thumbnail() ) { ?>

                            <div class="hero-image auction-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>'); ?>'">
                            </div>

                        <?php } else { ?>

                            <div class="hero-image auction-image" style="background-image: url('<?php echo esc_url( get_template_directory_uri()) . '/img/1x1.svg';?>')">
                            </div>

                        <?php }; ?>


                        <div class="hero-info">
                            <div>
                                <?php if( get_field('code') ): ?>
                                    <div class="hero-code"><?php the_field( 'code' ); ?></div>
                                <?php endif; ?>

                                <h2 class="hero-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                                <div class="hero-details">
                                    <?php if( get_field('date') ): ?>
                                        <div class="hero-date"><?php the_field( 'date' ); ?></div>
                                    <?php endif; ?>

                                    <?php if( get_field('time') ): ?>
                                        <div class="hero-time"><?php the_field( 'time' ); ?></div>
                                    <?php endif; ?>

                                    <?php if( get_field('address') ): ?>
                                        <div class="hero-location"><?php the_field( 'address' ); ?></div>
                                    <?php endif; ?>
                                </div>

                                <ul class="auction-nav">
                                    <li>
                                        <a href="<?php the_permalink(); ?>" class="btn">View auction</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
        <?php }
        } else {
            // no posts found
        }

        // Restore original Post Data
        wp_reset_postdata();
    ?>

</section><!-- /.hero -->