<section class="hero">
    <?php get_template_part('template-parts/components/lot', 'slider' ); ?>

    <div class="hero-info">
        <div>
            <?php if( get_field('lot_number') ): ?>
                <div class="lot-info hero-code">Lot #<?php the_field( 'lot_number' ); ?></div>
            <?php endif; ?>

            <?php if( get_field('artist') ): ?>
                <h2 class="lot-info hero-title"><?php the_field( 'artist' ); ?></h2>
            <?php endif; ?>

            <div class="lot-info hero-artwork"><?php the_field( 'artwork_title' ); ?></div>

            <?php if( get_field('artwork_specs') ): ?>
                <div class="lot-info hero-specs">
                    <?php the_field( 'artwork_specs' ); ?>
                </div>
            <?php endif; ?>


            <div class="lot-info hero-starting-bid">Starting Bid :
                <?php
                    $str = get_field('starting_bid');

                    if (preg_match('#[0-9]#',$str)){
                        echo 'Php ' . number_format((get_field('starting_bid')), 0, '.', ',');
                    } else {
                        echo get_field('starting_bid');
                    }
                ?>
            </div>



            <?php if( get_field('authenticity') || get_field('provenance') || get_field('exhibited') || get_field('literature') ): ?>

                <div class="lot-info hero-lot-details">
                    <?php if( get_field('authenticity') ): ?>
                        <div class="lot-authenticity">
                            <strong>Authenticity</strong>
                            <?php the_field( 'authenticity' ); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('provenance') ): ?>
                        <div class="lot-provenance">
                            <strong>Provenance</strong>
                            <?php the_field( 'provenance' ); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('exhibited') ): ?>
                        <div class="lot-exhibited">
                            <strong>Exhibited</strong>
                            <?php the_field( 'exhibited' ); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('literature') ): ?>
                        <div class="lot-literature">
                            <strong>Literature</strong>
                            <?php the_field( 'literature' ); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if( get_field('writeup') ): ?>
                <div class="lot-info hero-writeup">
                    <?php the_field( 'writeup' ); ?>
                </div>
            <?php endif; ?>
        </div>
    </div><!-- .hero-info -->
</section><!-- .hero-auction-slider -->