<section class="fs-bar">
    <div class="fs-bar-keyword">
        <label class="fs-bar-title">Search by Keyword</label>
        <div>
            <?php echo facetwp_display( 'facet', 'search_by_keyword' ); ?>
        </div>
    </div>
    <div class="fs-bar-lot">
        <label class="fs-bar-title">Search by Lot #</label>
        <div>
            <?php echo facetwp_display( 'facet', 'search_by_lot' ); ?>
        </div>
    </div>
    <div class="fs-bar-sort">
        <label class="fs-bar-title">Sort by</label>
        <div class="sort-wrapper">
            <?php echo facetwp_display( 'sort', 'true' ); ?>
        </div>
    </div>
</section>