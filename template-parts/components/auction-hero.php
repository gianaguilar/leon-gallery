<section class="hero">
    <div class="hero-info">
        <div>
            <?php if( get_field('code') ): ?>
                <div class="hero-code"><?php the_field( 'code' ); ?></div>
            <?php endif; ?>

            <h2 class="hero-title"><?php the_title(); ?></h2>

            <div class="hero-details">
                <?php if( get_field('date') ): ?>
                    <div class="hero-date"><?php the_field( 'date' ); ?></div>
                <?php endif; ?>

                <?php if( get_field('time') ): ?>
                    <div class="hero-time"><?php the_field( 'time' ); ?></div>
                <?php endif; ?>

                <?php if( get_field('address') ): ?>
                    <div class="hero-location"><?php the_field( 'address' ); ?></div>
                <?php endif; ?>
            </div>

            <ul class="auction-nav">
                <?php if( get_field('e-catalogue') ): ?>
                    <li>
                        <a href="<?php the_field('e-catalogue'); ?>" target="_blank" class="btn">E-Catalogue</a>
                    </li>
                <?php endif; ?>

                <?php if ( get_field( 'upload_pdf_catalog' ) ) { ?>
                    <li>
                        <a href="<?php the_field( 'upload_pdf_catalog' ); ?>" class="hero-auction-view-catalogue btn" target="_blank" download>Download PDF</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section><!-- /.hero -->