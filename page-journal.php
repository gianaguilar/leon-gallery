<?php
/*
    Template Name: Journal
*/
get_header(); ?>


<main>
    <?php get_template_part('template-parts/components/top', 'bar' ); ?>

    <section class="section journal">
        <div class="index-container">
            <header>
                <h2 class="section-title">Journal Entries</h2>
            </header><!-- /header -->

            <div class="box-container">
                <?php echo do_shortcode( '[ajax_load_more container_type="div" css_classes="journal-listing" preloaded="true" preloaded_amount="9" seo="true" repeater="template_2" post_type="post" posts_per_page="9" pause="true" scroll="false" button_label="Load More" button_loading_label="Loading..."]' ); ?>

                <div class="box box-thirds no-height"></div>
                <div class="box box-thirds no-height"></div>
                <div class="box box-thirds no-height"></div>
            </div>
        </div>
    </section>
</main>


<?php get_footer(); ?>