jQuery(document).ready(function($){
    // Menu Button
    // =================================================
    $('.top-bar-menu').click(function () {
        $('.hamburger').toggleClass('close');
        $('#menu-overlay').toggleClass('isOpen');

        $('body').toggleClass('menu-overlay-isOpen');
    });


    // Makes Flexslider
    // =================================================
    var heroHeight = $(window).height() - 70;
    $('.lot-image').css({"height": heroHeight, "max-height": heroHeight});
    $('.lot-image img').css('max-height', heroHeight - 100);

    $(window).resize(function() {
        var heroHeight = $(window).height() - 70;
        $('.lot-image').css({"height": heroHeight, "max-height": heroHeight});
        $('.lot-image img').css('max-height', heroHeight - 100);
    });

    // Lot Slider
    // =================================================
    if ($('.lot-slider').length > 0) {
        var Len = $('.lot-slider li').length;

        if(Len >1){
            swipe = true;
        }
        else{
           swipe = false;
        }
        $('.lot-slider').flexslider({
            animation: "fade",
            touch:swipe,
            after: function(slide) {
                //console.log(slider....);
            }
        });
    }

    // Auction Flexslider
    // =================================================
    $('.auction-flexslider').flexslider({
        animation: "fade",
        controlNav: false
    });

    // Magnific
    // =================================================
    $('.popup-gallery').magnificPopup({
        delegate: '.gallery-link',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
    });

    // Smooth Scroll
    // https://codepen.io/gianaguilar/pen/YxeJVO
    // =================================================
    // $('.scrollTarget').click(function(event) {
    //     // On-page links
    //     if ( location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname ) {

    //         var target = $(this.hash);
    //         target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

    //         if (target.length) {
    //             event.preventDefault();
    //             $('html, body').animate({
    //                     scrollTop: target.offset().top - 70 // Topbar Height
    //                 }, 300, function() {
    //             });
    //         }
    //     }
    // });



    // Custom Select
    // =================================================
        $('select').selectric({
            disableOnMobile: false,
            nativeOnMobile: false,
        });

        $(document).on('facetwp-loaded', function() {
            $('select').selectric({
                disableOnMobile: false,
                nativeOnMobile: false,
            });
        });


    $(document).on('facetwp-loaded', function() {
        var searchKeyword = $('.fs-bar-keyword .facetwp-search').val();
        var searchLot = $('.fs-bar-lot .facetwp-search').val();

        if (searchKeyword.length > 0 || searchLot.length > 0) {
            $('.search-title').html('</br>Results for: ');
        } else {
            $('.search-title').html('');
        }

        if (searchKeyword.length > 0 && searchLot.length > 0) {
            $('.search-results').html('<span>' + searchKeyword + '</span>' + '<span>, ' + searchLot + '</span>');
        } else if (searchKeyword.length > 0 && searchLot.length === 0) {
            $('.search-results').html('<span>' + searchKeyword + '</span>');
        } else if (searchKeyword.length === 0 && searchLot.length > 0) {
            $('.search-results').html('<span>' + searchLot + '</span>');
        } else {
            $('.search-results').html('');
        }
    });

    // Headroom
    // =================================================
    $("#top-bar").headroom({
        "offset": 30,
        "tolerance": 3,
    });
});