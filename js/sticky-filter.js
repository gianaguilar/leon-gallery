jQuery(document).ready(function($) {
    // Sticky Search Sort Bar
    var distance = $('.wrapper').offset().top;
        $window = $(window);


    $window.scroll(function() {
        if ( $window.scrollTop() >= distance ) {
            $('.fs-wrapper').addClass('affix');
            $('.top-bar').addClass('isHidden');
        }
        else {
            $('.fs-wrapper').removeClass('affix');
            $('.top-bar').removeClass('isHidden');
        }
    });

    $('.fs-trigger').click(function() {
        $('.fs-trigger').toggleClass('isClicked');
        $('.fs-wrapper').toggleClass('isOpen');
        $('body').toggleClass('fs-isOpen');
    });

    $('.fs-bg').click(function() {
        $('.fs-trigger').toggleClass('isClicked');
        $('.fs-wrapper').toggleClass('isOpen');
        $('body').toggleClass('fs-isOpen');
    });
});